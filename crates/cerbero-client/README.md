#cerbero client

cerbero client is lib to consume the service at frontend.

actually is built over yew

# Usage

1. Add the dependency to your Cargo.toml
```toml
[dependencies.cerbero-client]
name = "cerbero"
version = "0.1.0"
git = "some-url"
```


2. Wrap your app in the provider component
```rust
//in your app.rs (or whatever be the entry point of your app)
use yew::prelude::*;
use cerbero::prelude::*;


#[function_component(App)]
pub fn app() -> Html {
	html! {
		<AuthProvider> //auth provider component
			<div>
				{"your app here"}
			</div>
		</AuthProvider>
	}
}
```

3. that's it!, now your ready to use login, register, and logout components

