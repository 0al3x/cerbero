#cerbero provider

cerbero provider is the server that runs your auth service

# usage

1. install the crate
```bash
cargo install cerbero-provider
```

or built manally
```bash
# clone the repo
git clone cerbero-provider-git-url

cd /path/to/cerbero-provider #cd ./cerbero-provider

cargo build --release
```

2. optional you can pass it a Config.toml file

3. run it!
```bash
cerbero-service
```
